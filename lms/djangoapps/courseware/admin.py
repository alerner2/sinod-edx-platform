'''
django admin pages for courseware model
'''

from courseware.models import StudentModule, OfflineComputedGrade, OfflineComputedGradeLog, CourseCollections
from ratelimitbackend import admin

class CourseCollectionsAdmin(admin.ModelAdmin):
    list_display = ['parent', 'child', 'is_active', 'created_by', 'created_at']
    search_fields = ['parent', 'child']

    def save_model(self, request, obj, form, change):
        if not change:
            obj.created_by = request.user
        obj.save()

    def get_form(self, request, obj=None, **kwargs):
        self.exclude = ("is_retroactive", )
        form = super(CourseCollectionsAdmin, self).get_form(request, obj, **kwargs)
        return form

admin.site.register(StudentModule)

admin.site.register(OfflineComputedGrade)

admin.site.register(OfflineComputedGradeLog)

admin.site.register(CourseCollections, CourseCollectionsAdmin)
